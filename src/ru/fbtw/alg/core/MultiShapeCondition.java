package ru.fbtw.alg.core;

import ru.fbtw.alg.shapes.Shape;

import java.util.ArrayList;

public class MultiShapeCondition extends ShapeCondition {
    private ArrayList<SimpleShapeCondition> conditions;
    private Colors color;

    private MultiShapeCondition() {}

    @Override
    public boolean getResult(double x, double y){
        boolean result = false;

        for (SimpleShapeCondition condition : conditions) {
            result = condition.getResult(x,y) || result;
        }

        return result;
    }

    public Colors getColor() {
        return color;
    }

    public static MultiShapeConditionBuilder newBuilder() {
        return new MultiShapeCondition().new MultiShapeConditionBuilder();
    }

    public class MultiShapeConditionBuilder {
        private ArrayList<Shape> internal;
        private ArrayList<Shape> external;

        private MultiShapeConditionBuilder(){
            MultiShapeCondition.this.conditions = new ArrayList<>();

            internal = new ArrayList<>();
            external = new ArrayList<>();

        }

        public MultiShapeConditionBuilder andInternal(Shape shape){
            internal.add(shape);
            return this;
        }
        public MultiShapeConditionBuilder andExternal(Shape shape){
            external.add(shape);
            return this;
        }

        public MultiShapeConditionBuilder or(){
            MultiShapeCondition
                    .this
                    .conditions
                    .add(new SimpleShapeCondition(internal,external));

            internal.clear();
            external.clear();

            return this;
        }

        public MultiShapeConditionBuilder setColor(Colors color){
            MultiShapeCondition.this.color = color;
            return this;
        }

        public MultiShapeCondition build(){
            return MultiShapeCondition.this;
        }

    }
}
