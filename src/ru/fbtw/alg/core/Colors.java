package ru.fbtw.alg.core;

import java.awt.*;

public enum Colors {
    RED,
    GREEN,
    YELLOW,
    WHITE,
    BLUE,
    ORANGE;

    public  Color toCommon() {
        switch (this) {
            case RED:
                return Color.RED;
            case BLUE:
                return Color.BLUE;
            case GREEN:
                return Color.GREEN;
            case WHITE:
                return Color.WHITE;
            case ORANGE:
                return Color.ORANGE;
            case YELLOW:
                return Color.YELLOW;
        }
        return null;
    }

}
