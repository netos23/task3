package ru.fbtw.alg.core;

public abstract class ShapeCondition {
    public abstract boolean getResult(double x, double y);
}
