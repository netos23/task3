package ru.fbtw.alg.core;

import ru.fbtw.alg.shapes.Shape;

import java.util.ArrayList;

public class SimpleShapeCondition extends ShapeCondition {
    private ArrayList<Shape> internal;
    private ArrayList<Shape> external;

    public SimpleShapeCondition(ArrayList<Shape> internal, ArrayList<Shape> external) {
        this.internal = new ArrayList<>(internal);
        this.external = new ArrayList<>(external);
    }


    public boolean getResult(double x, double y) {
        boolean result = true;

        for (Shape shape : internal) {
            result = result && shape.isInternalPoint(x,y);
        }

        for (Shape shape : external) {
            result = result && shape.isExternalPoint(x,y);
        }
        return result;
    }

}
