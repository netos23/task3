package ru.fbtw.alg.shapes;

public abstract class Parabola extends  Shape {

    double a,b,c;

    Parabola(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

}
