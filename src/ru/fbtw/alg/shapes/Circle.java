package ru.fbtw.alg.shapes;

import static ru.fbtw.alg.utils.MathUtils.*;

public class Circle extends Shape {

    private double radius;

    public Circle(double x, double y, double radius) {
        this.x = x;
        this.y = y;
        this.radius = radius;
    }

    @Override
    public boolean isInternalPoint(double x, double y) {
        return sqr(x-this.x) + sqr(y - this.y) <= sqr(radius);
    }
}
