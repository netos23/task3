package ru.fbtw.alg.shapes;

import static ru.fbtw.alg.utils.MathUtils.*;

public class VerticalParabola extends Parabola {

    public VerticalParabola(double a, double b, double c) {
        super(a, b, c);
    }

    public static VerticalParabola fromPoints(double[] points) {
        double[][][] matrix = new double[4][3][3];
        int lockColumn = -1;

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                for (int k = 0; k < matrix[0][0].length; k++) {
                    if (lockColumn != k) {
                        switch (k) {
                            case 0:
                                matrix[i][j][k] = sqr(points[j * 2]);
                                break;
                            case 1:
                                matrix[i][j][k] = points[j * 2];
                                break;
                            case 2:
                                matrix[i][j][k] = 1;
                                break;
                        }
                    } else {
                        matrix[i][j][k] = points[j * 2 + 1];
                    }
                }
            }
            lockColumn++;
        }
        double mainDeterminant = getDeterminant(matrix[0]);

        double a = getDeterminant(matrix[1]) / mainDeterminant;
        double b = getDeterminant(matrix[2]) / mainDeterminant;
        double c = getDeterminant(matrix[3]) / mainDeterminant;

        return new VerticalParabola(a, b, c);
    }

    @Override
    public boolean isInternalPoint(double x, double y) {
        return y >= a * sqr(x) + b * x + c;
    }
}
