package ru.fbtw.alg.shapes;

import static ru.fbtw.alg.utils.MathUtils.getDeterminant;
import static ru.fbtw.alg.utils.MathUtils.sqr;

public class Line extends Shape {
    private double k,m;

    public Line(double k, double m) {
        this.k = k;
        this.m = m;
    }

    public static Line fromPoints(double[] points){
        double [][][] matrix = new double[3][2][2];

        int lockColumn = -1;

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                for (int k = 0; k < matrix[0][0].length; k++) {
                    if (lockColumn != k) {
                        switch (k) {
                            case 0:
                                matrix[i][j][k] = points[j * 2];
                                break;
                            case 1:
                                matrix[i][j][k] = 1;
                                break;
                        }
                    } else {
                        matrix[i][j][k] = points[j * 2 + 1];
                    }
                }
            }
            lockColumn++;
        }

        double mainDeterminant = getDeterminant(matrix[0]);

        double a = getDeterminant(matrix[1]) / mainDeterminant;
        double b = getDeterminant(matrix[2]) / mainDeterminant;


        return new Line(a, b);
    }

    @Override
    public boolean isInternalPoint(double x, double y) {
        return y >= k * x + m;
    }
}
