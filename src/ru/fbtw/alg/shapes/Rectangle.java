package ru.fbtw.alg.shapes;

public class Rectangle extends Shape{

    private double width, height;

    public Rectangle(double x, double y, double width, double height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public static Rectangle fromPoints(double x1, double y1, double x2, double y2){
        double width = Math.abs(x1-x2);
        double height = Math.abs(y1-y2);

        double[] origin = getOriginPoint(x1, y1, x2, y2);

        return new Rectangle(origin[0],origin[1],width,height);
    }

    private static double[] getOriginPoint(double x1, double y1, double x2, double y2){
        double [] point = new double[2];

        point[0] = x1 > x2 ? x2 : x1;
        point[1] = y1 > y2 ? y2 : y1;

        return point;
    }


    @Override
    public boolean isInternalPoint(double x, double y) {
        boolean horCondition = this.x <= x && Math.abs(x - this.x) <= width;
        boolean vertCondition = this.y <= y && Math.abs(y - this.y) <= height;
        return horCondition && vertCondition;
    }


}
