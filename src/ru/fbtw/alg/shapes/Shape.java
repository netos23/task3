package ru.fbtw.alg.shapes;

public abstract class Shape {
    double x,y;

    public abstract boolean isInternalPoint(double x, double y);

    public boolean isExternalPoint(double x, double y){
        return !isInternalPoint(x,y);
    }
}
