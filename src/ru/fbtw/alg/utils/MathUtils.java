package ru.fbtw.alg.utils;

public class MathUtils {
    public static double sqr(double x){
        return x*x;
    }

    public static double getDeterminant(double [][] matrix) {
        if (matrix.length != matrix[0].length) return Double.NaN;

        if (matrix.length == 2) {
            return matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0];
        } else {

            double determinant = 0;

            for (int i = 0; i < matrix.length; i++) {

                double[][] minor = new double[matrix.length - 1][matrix.length - 1];
                int i1 = 0, j1 = 0;
                
                for (int j = 1; j < matrix.length; j++) {
                    for (int k = 0; k < matrix.length; k++) {
                        if (k != i) {
                            minor[i1][j1] = matrix[j][k];
                            j1++;
                        }
                    }
                    i1++;
                    j1 = 0;
                }

                int kof = (i % 2 == 0) ? 1 : -1;

                determinant += kof * matrix[0][i] * getDeterminant(minor);
            }

            return determinant;
        }
    }
}
