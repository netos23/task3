package ru.fbtw.alg;

import ru.fbtw.alg.core.Colors;
import ru.fbtw.alg.core.MultiShapeCondition;
import ru.fbtw.alg.io.MultiShapeConditionReader;
import ru.fbtw.alg.io.ShapeReader;
import ru.fbtw.alg.shapes.Shape;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.TreeMap;



public class Main extends Canvas{
    private static ArrayList<MultiShapeCondition> conditions;
    private static Colors defaultColor;

    private final static int IMAGE_HEIGHT = 800, IMAGE_WIDTH = 800;
    private final static int CANVAS_ABS_SIZE = 10;
    private BufferedImage img;

    @Override
    public void paint(Graphics g) {
        int x_unit = IMAGE_WIDTH / CANVAS_ABS_SIZE / 2, y_unit = IMAGE_HEIGHT / CANVAS_ABS_SIZE / 2;
        for (int x = 0; x < IMAGE_WIDTH; x++) {
            for (int y = 0; y < IMAGE_HEIGHT; y++) {
                double canv_x = (double) x / x_unit - CANVAS_ABS_SIZE;
                double canv_y = CANVAS_ABS_SIZE - (double) y / y_unit;
                if (x % x_unit == 0 || y % y_unit == 0) {
                    img.setRGB(x, y, Color.GRAY.getRGB());
                } else {
                    img.setRGB(x, y, getColor(canv_x, canv_y).toCommon().getRGB());
                }
            }
        }
        g.drawImage(img, 0, 0, null);
        // оси координат
        g.setColor(Color.BLACK);
        ((Graphics2D)g).setStroke(new BasicStroke(2));
        g.drawLine(IMAGE_WIDTH / 2, 0, IMAGE_WIDTH / 2, IMAGE_HEIGHT);
        g.drawLine(0, IMAGE_HEIGHT / 2, IMAGE_WIDTH, IMAGE_HEIGHT / 2);
    }

    public Main() {
        img = new BufferedImage(IMAGE_WIDTH, IMAGE_HEIGHT, BufferedImage.TYPE_INT_ARGB);
        initZones();

    }

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        Main canvas = new Main();
        canvas.setSize(IMAGE_WIDTH, IMAGE_HEIGHT);
        frame.add(canvas);
        frame.pack();
        frame.setVisible(true);
        /*Locale.setDefault(Locale.ROOT);
        initZones();
        Scanner in = new Scanner(System.in);

        printPoint(0,0);
        printPoint(-3,0);
        printPoint(-1,2);
        printPoint(-1,5);
        printPoint(1.5,4.5);
        printPoint(1.5,7.1);

        System.out.println("Введите координаты случайной точки?");
        printPoint(in.nextDouble(),in.nextDouble());*/



    }

    private static void printPoint(double x, double y) {
        Colors color = getColor(x, y);
        System.out.printf("( %.2f ; %.2f ) -> %s\n", x, y, color);
    }

    public static Colors getColor(double x, double y){
        for(MultiShapeCondition condition : conditions){
            if(condition.getResult(x,y)){
                return condition.getColor();
            }
        }
        return defaultColor;
    }

    private static void initZones() {
        try {
            Scanner shapeInput = new Scanner(new File("shape.input"));
            Scanner zoneInput = new Scanner(new File("zone.input"));

            TreeMap<String, Shape> shapes = new ShapeReader(shapeInput).readShapeMap();
            MultiShapeConditionReader reader = new MultiShapeConditionReader(shapes, zoneInput);

            defaultColor = reader.readColor();
            conditions = reader.readAllConditions();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }


}



