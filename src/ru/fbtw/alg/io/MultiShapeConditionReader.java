package ru.fbtw.alg.io;

import ru.fbtw.alg.core.Colors;
import ru.fbtw.alg.core.MultiShapeCondition;
import ru.fbtw.alg.shapes.Shape;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.TreeMap;


/**
 * DEFAULT COLOR
 *
 * COUNT OF ZONES
 *
 * COLOR
 * CONDITION COUNT  i_ or e_ for internal or external part + id + & for connect zones another conditions
 *
 * */

public class MultiShapeConditionReader {
    private static final String SEPORATOR = "&";
    private TreeMap<String, Shape> shapes;
    private Scanner in;

    public MultiShapeConditionReader(TreeMap<String, Shape> shapes, Scanner in) {
        this.shapes = shapes;
        this.in = in;
    }

    public MultiShapeCondition readCondition() {
        Colors zoneColor = readColor();
        int conditionCount = in.nextInt();
        MultiShapeCondition.MultiShapeConditionBuilder builder = MultiShapeCondition.newBuilder();
        builder.setColor(zoneColor);

        if(zoneColor == null) return null;

        for (int i = 0; i < conditionCount; i++) {
            String[] tmp = in.next().split(SEPORATOR);
            for (String condition : tmp) {
                String shapeName = condition.substring(2);
                if (shapes.containsKey(shapeName)) {
                    if (condition.charAt(0) == 'i') {
                        builder.andInternal(shapes.get(shapeName));
                    } else if (condition.charAt(0) == 'e') {
                        builder.andExternal(shapes.get(shapeName));
                    } else {
                        System.out.println("Condition skipped: wrong code ");
                        break;
                    }
                }else{
                    System.out.println("Condition skipped: unknown shape ");
                }
            }
            builder.or();
        }

        return builder.build();
    }

    public Colors readColor() {
        String color = in.next();
        return Colors.valueOf(color);
    }

    public ArrayList<MultiShapeCondition> readAllConditions(){
        int count = in.nextInt();
        ArrayList<MultiShapeCondition> conditions = new ArrayList<>();
        for (int i =0;i<count;i++){
            MultiShapeCondition tmp = readCondition();
            if(conditions != null){
                conditions.add(tmp);
            }
        }

        return conditions;
    }
    
    
}
