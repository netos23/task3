package ru.fbtw.alg.io;


/**
 *
 * shape count
 *
 * id type [constructor type] [other flags]
 * param0 param1 param....
 *
 *
 *
 * */

import ru.fbtw.alg.shapes.*;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.function.Supplier;

public class ShapeReader {
    private Scanner in;
    private ArrayList<Supplier<Shape>> methods;

    public ShapeReader(Scanner in) {
        this.in = in;
        methods = new ArrayList<>();

        methods.add(this::readLine);
        methods.add(this::readRectangle);
        methods.add(this::readCircle);
        methods.add(this::readParabola);

    }

    // 0 - line; 1 - rectangle; 2 - circle; 3 - parabola
    public int readShapeType() {
        return in.nextInt();
    }

    // 0 - from points 1 - from params
    public boolean readShapeConstructorType() {
        return in.nextInt() == 1;

    }

    public Rectangle readRectangle(boolean constructorType) {

        double a = in.nextDouble(); // x for constructor or x1 from firstPoint
        double b = in.nextDouble(); // y for constructor or y1 from firstPoint

        double c = in.nextDouble(); // width or x2 from secondPoint
        double d = in.nextDouble(); // height or y2 from secondPoint

        return constructorType ? new Rectangle(a, b, c, d) : Rectangle.fromPoints(a, b, c, d);
    }

    public Rectangle readRectangle() {
        boolean constructorType = readShapeConstructorType();
        return readRectangle(constructorType);
    }

    public Circle readCircle() {

        double x = in.nextDouble();
        double y = in.nextDouble();
        double r = in.nextDouble();

        return new Circle(x, y, r);
    }

    public Parabola readParabola(boolean constructorType) {

        int type = in.nextInt(); // 0 for horizontal; 1 for vertical;

        if (constructorType) {

            double a = in.nextDouble();
            double b = in.nextDouble();
            double c = in.nextDouble();

            return type == 0
                    ? new VerticalParabola(a, b, c)
                    : new HorizontalParabola(a, b, c);
        } else {

            double[] points = readPoints(3);

            return type == 0
                    ? VerticalParabola.fromPoints(points)
                    : HorizontalParabola.fromPoints(points);
        }
    }

    public Parabola readParabola() {
        boolean constructorType = readShapeConstructorType();
        return readParabola(constructorType);
    }

    public Line readLine(boolean constructorType) {
        if (constructorType) {

            double k = in.nextDouble();
            double m = in.nextDouble();

            return new Line(k, m);
        } else {

            double[] points = readPoints(2);
            return Line.fromPoints(points);
        }
    }

    private Line readLine() {
        boolean constructorType = readShapeConstructorType();
        return readLine(constructorType);
    }

    private double[] readPoints(int count) {
        double[] points = new double[count * 2];

        for (int i = 0; i < points.length; i++) {
            points[i] = in.nextDouble();
        }

        return points;
    }

    public TreeMap<String, Shape> readShapeMap() {
        int count = in.nextInt();
        TreeMap<String, Shape> result = new TreeMap<>();

        for (int i = 0; i < count; i++) {
            String id = in.next();
            int type = readShapeType();

            Shape target = methods.get(type).get();

            result.put(id, target);
        }

        return result;
    }


}



